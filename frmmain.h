/*************************************************************************
    > File Name: frmmain.h
    > Author: ma6174
    > Mail: ma6174@163.com 
    > Created Time: 2015年03月19日 星期四 09时07分02秒
 ************************************************************************/

#include<iostream>
#include <wx/frame.h>
#include <wx/bmpbuttn.h>
#include "card.h"
#include "cardslot.h"
#include "globle.h"
class frmMain:public wxFrame
{
public:
	frmMain();
private:
	Card *btn;
	CardSlot *cardSlot_b;
	CardSlot *cardSlot_l;
	CardSlot *cardSlot_r;
	void OnPaint(wxPaintEvent &event);
	void OnPaint(wxSizeEvent &event);
	void OnMouseUp(wxMouseEvent &event);
    void OnDClick(wxMouseEvent &event);
	wxDECLARE_EVENT_TABLE();
};
