/*************************************************************************
    > File Name: pos_and_size.cpp
    > Author: scma
    > Mail: scma@kingbase.com.cn 
    > Created Time: 2015年04月02日 星期四 14时42分08秒
 ************************************************************************/
#include "pos_and_size.h"
GenFrame* GenFrame::_instance = 0;
GenFrame* GenFrame::Instance()
{
	if(_instance == 0)
	{
		_instance = new GenFrame();
	}
	return _instance;
}

void GenFrame::SetWindow(wxWindow* win)
{
	this -> parent = win;
}
int GenFrame::getCardSlip()
{
	int ret;
	ret = parent->GetSize().GetHeight()/30;
	return ret;
}
wxPoint GenFrame::getButtomSlotPos()
{
	wxSize S = parent->GetSize();
	int width ,height;
	width = S.GetWidth();
	height= S.GetHeight();
	wxPoint ret;
	ret.x = width/8*1;
	ret.y = height/8*6;
	return ret;
}

wxPoint GenFrame::getRightSlotPos()
{
    wxSize S = parent->GetSize();
	int width ,height;
	width = S.GetWidth();
	height= S.GetHeight();
	wxPoint ret;
	ret.x = width/20*19 - 50 - 5;
	ret.y = height/10*1;
	return ret;
}

wxPoint GenFrame::getLeftSlotPos()
{
	wxSize S = parent->GetSize();
	int width ,height;
	width = S.GetWidth();
	height= S.GetHeight();
	wxPoint ret;
	ret.x = width/20*1;
	ret.y = height/10*1;
	return ret;
}

wxSize  GenFrame::getLeftSlotSize()
{
	int width ,height;
	wxSize ret;
	wxSize S = parent->GetSize();
	width = S.GetWidth()/5*1;
	height= S.GetHeight()/7*4;
	ret.SetWidth(width);
	ret.SetHeight(height);
	return ret;
}

wxSize  GenFrame::getButtomSlotSize()
{
	int width ,height;
	wxSize ret;
	wxSize S = parent->GetSize();
	width = S.GetWidth()/4*3;
	height= S.GetHeight()/4;
	ret.SetWidth(width);
	ret.SetHeight(height);
	return ret;
}

wxSize  GenFrame::getRightSlotSize()
{
    int width ,height;
	wxSize ret;
	wxSize S = parent->GetSize();
	width = S.GetWidth()/5*1;
	height= S.GetHeight()/7*4;
	ret.SetWidth(width);
	ret.SetHeight(height);
	return ret;
}

wxPoint GenFrame::getUpSlotPos()
{
	;
}

wxSize  GenFrame::getUpSlotSize()
{
	;
}

GenFrame::GenFrame()
{
	;
}

GenFrame::~GenFrame()
{
	;
}

