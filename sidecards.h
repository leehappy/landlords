/*************************************************************************
    > File Name: sidecards.h
    > Author: superman_msc
    > Mail:   981880785@qq.com
    > Created Time: 2015年04月02日 星期四 15时58分17秒
 ************************************************************************/
#ifndef _H_SUPERMAN_MSC_SIDE_CARDS_H
#define _H_SUPERMAN_MSC_SIDE_CARDS_H
#include <wx/panel.h>
#include "card.h"
class SideCards
{
public:
	SideCards(wxWindow *parent,
			 wxWindowID id = wxID_ANY,
			 const wxPoint& pos = wxDefaultPosition,
			 const wxSize& size = wxDefaultSize);
	AddCard(Card *newcard);
	Slot();
private:
    CardList cardlist;
	wxSize cardSize;
}
#endif //_H_SUPERMAN_MSC_SIDE_CARDS_H
