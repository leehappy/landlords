/*************************************************************************
    > File Name: cardslot.h
    > Author: superman_msc
    > Mail:   981880785@qq.com
    > Created Time: 2015年03月26日 星期四 16时42分01秒
 ************************************************************************/
#ifndef _H_CARDSLOT_SUPERMAN_MSC_H
#define _H_CARDSLOT_SUPERMAN_MSC_H
#include<iostream>
#include "card.h"
#include <wx/panel.h>

class CardSlot : public wxPanel
{
public:
    enum SLOT_POS{
        BUTTOM = 0,
        LEFT,
        RIGHT
    };
	CardSlot(wxWindow *parent,
			 wxWindowID id = wxID_ANY,
			 const wxPoint& pos = wxDefaultPosition,
			 const wxSize& size = wxDefaultSize);
	void AddCard(Card *newCard);
    void ClearChose();
	void Slot();
    void SetSlotPos(SLOT_POS pos);
private:
	wxSize cardSize;
	CardList cardlist;
    SLOT_POS SlotPosStyle;

	//SortCards();
	//AddCards(CardList * list);
};
#endif //_H_CARDSLOT_SUPERMAN_MSC_H

