/*************************************************************************
    > File Name: pos_and_size.h
    > Author: superman_msc
    > Mail:   981880785@qq.com
    > Created Time: 2015年04月02日 星期四 14时26分22秒
 ************************************************************************/
#ifndef _H_SUPERMAN_POSAND_SIZE_H
#define _H_SUPERMAN_POSAND_SIZE_H
#include <wx/wx.h>
#include<wx/gdicmn.h>
#include<wx/frame.h>
//全局类，记录各个窗口位置大小
class GenFrame
{
public:
	static GenFrame* Instance();
	~GenFrame();
	int     getCardSlip();
	wxPoint getButtomSlotPos();
	wxSize  getButtomSlotSize();

	wxPoint getLeftSlotPos();
	wxSize  getRightSlotSize();

	wxPoint getUpSlotPos();
	wxSize  getUpSlotSize();
    wxPoint getRightSlotPos();
    wxSize  getLeftSlotSize();
	void SetWindow(wxWindow *win);
protected:
	GenFrame();
private:
	static GenFrame* _instance;
	wxSize frameSize;
	wxWindow *parent;
};


#endif ///_H_SUPERMAN_POSAND_SIZE_H


