/*************************************************************************
    > File Name: frmmain.cpp
    > Author: ma6174
    > Mail: ma6174@163.com
    > Created Time: 2015年03月19日 星期四 09时07分47秒
 ************************************************************************/

#include<iostream>
#include<wx/sizer.h>
#include "frmmain.h"
#include "card.h"
#include "cardslot.h"
#include <wx/bitmap.h>
#include <wx/image.h>
frmMain::frmMain()
	:wxFrame((wxWindow*)NULL,1,wxT("landlord"))
{
	wxInitAllImageHandlers();
	wxImage i(wxT("scr.png"));
	wxBitmap b(i);
	cardSlot_b = new CardSlot(this,wxID_ANY,wxPoint(0,0),this->GetSize());
	cardSlot_b->SetSlotPos(CardSlot::BUTTOM);
	cardSlot_l = new CardSlot(this,wxID_ANY,wxPoint(0,0),this->GetSize());
	cardSlot_l->SetSlotPos(CardSlot::LEFT);
	cardSlot_r = new CardSlot(this,wxID_ANY,wxPoint(0,0),this->GetSize());
	cardSlot_r->SetSlotPos(CardSlot::RIGHT);
	for(int i=0;i<27;i++)
	{
		btn = new Card(cardSlot_b,wxID_ANY,b,wxPoint(0,0));
		cardSlot_b->AddCard(btn);

		btn = new Card(cardSlot_b,wxID_ANY,b,wxPoint(0,0));
		cardSlot_r->AddCard(btn);

        btn = new Card(cardSlot_b,wxID_ANY,b,wxPoint(0,0));
		cardSlot_l->AddCard(btn);
	}
}

void frmMain::OnPaint(wxSizeEvent &event)
{
	cardSlot_b->SetSize(genframe->getButtomSlotSize());
	cardSlot_b->SetPosition(genframe->getButtomSlotPos());
	cardSlot_b->Slot();
    cardSlot_b->ClearChose();

    cardSlot_l->SetSize(genframe->getLeftSlotSize());
	cardSlot_l->SetPosition(genframe->getLeftSlotPos());
	cardSlot_l->Slot();

    cardSlot_r->SetSize(genframe->getRightSlotSize());
	cardSlot_r->SetPosition(genframe->getRightSlotPos());
	cardSlot_r->Slot();
}
void frmMain::OnPaint(wxPaintEvent &event)
{
	cardSlot_b->Update();
}
void frmMain::OnMouseUp(wxMouseEvent &event)
{
	cardSlot_b->Update();
}

void frmMain::OnDClick(wxMouseEvent &event){
    cardSlot_b->ClearChose();
}

wxBEGIN_EVENT_TABLE(frmMain, wxFrame)
	EVT_PAINT(    frmMain::OnPaint)
	EVT_LEFT_UP(  frmMain::OnMouseUp)
	EVT_SIZE(     frmMain::OnPaint)
    EVT_LEFT_DCLICK(frmMain::OnDClick)
wxEND_EVENT_TABLE()
