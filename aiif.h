
#ifndef _H_AIIF_SUPERMAN_MSC
#define _H_AIIF_SUPERMAN_MSC_
#include "card.h"
#define CARDLIST cardlist
#define OUT
#define IN
#define OUTED
#define OWNED
enum ROLETYPE
{
	landholder = 1,
	farmer
};
class aiif
{
public:
	virtual bool IsValid()
	{
		return false;
	}
	//ai 类型
	virtual ROLETYPE GetRoleType(){
		return 0;
	}
	//设置手里的牌。
	virtual void SetOwnedCards(CARDLIST *) = 0;
	//得到ai结果。
	virtual CARDLIST *GetAIResult() = 0;
	//设置等级。
	virtual void SetLevel() = 0;
	//所有以打出的牌。
	static virtual SetAllOutCards() = 0;
	//明牌/
	virtual SetAllInCards() = 0;
protect:
	//virtual deco for abstruct class
	aiif();
	virtual ~aiif();
	virtual int EvaluateCost(CARDLIST *pre,CARDLIST *next) = 0;
	virtual OWNED CARDLIST *NextOwnedCards(CARDLIST *out){
	virtual int EvaluateCurrent(CARDLIST OWNED *own,CARDLIST OUTED *outed) = 0;
	virtual int EvaluateCurrentStep(CARDLIST OWNED *owned, CARDLIST OUTED *outed, CARDLIST OUT *out) = 0;
		return 0;
	}
}
#endif //_H_AIIF_SUPERMAN_MSC_

