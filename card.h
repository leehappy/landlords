/*************************************************************************
    > File Name: card.h
    > Author: superman_msc
    > Mail:   981880785@qq.com
    > Created Time: 2015年03月19日 星期四 09时26分17秒
 ************************************************************************/
#ifndef _H_SUPERMAN_MSC_CARD_H
#define _H_SUPERMAN_MSC_CARD_H
#include<iostream>
#include <wx/wx.h>
#include <wx/control.h>
#include <wx/bmpbuttn.h>
#include <wx/list.h>
class Card:public wxWindow
{
public:
	Card(wxWindow *parent,
			wxWindowID id,
			const wxBitmap &bitmap,
			const wxPoint &pos=wxDefaultPosition,
			const wxSize &size = wxDefaultSize);
    void SetHide(bool hide);
    void Reset();
private:
	void OnEnter(wxMouseEvent& event);
	void OnLeave( wxMouseEvent& evnet);
    void OnPaint(wxPaintEvent &event);
    void OnMouseUP(wxMouseEvent &event);
    void OnDClick(wxMouseEvent &event);
    bool BeHide;
    bool IsChose;
	wxDECLARE_EVENT_TABLE();
};
WX_DECLARE_LIST(Card,CardList);
#endif //_H_SUPERMAN_MSC_CARD_H
