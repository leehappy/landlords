/*************************************************************************
    > File Name: cardslot.cpp
    > Author: superman_msc
    > Mail:   981880785@qq.com
    > Created Time: 2015年03月26日 星期四 16时48分07秒
 ************************************************************************/

#include<iostream>
#include "cardslot.h"
#include"globle.h"

CardSlot::CardSlot(wxWindow* parent,
                   wxWindowID id,
                   const wxPoint& pos,
                   const wxSize& size)
    :wxPanel(parent,id,pos,size)
{
    wxImage i(wxT("scr.png"));
    cardSize = i.GetSize();
    SlotPosStyle = BUTTOM;
}
void CardSlot::Slot()
{
    if(SlotPosStyle == BUTTOM){
        int endline = this->GetSize().GetWidth() - cardlist.GetFirst()->GetData()->GetSize().GetWidth();
        int cardnum = cardlist.GetCount();
        int slack = endline/cardnum;
        CardList::iterator iter;
        cardnum = 0;
        for(iter = cardlist.begin(); iter != cardlist.end(); ++iter)
        {
            Card *current = *iter;
            wxPoint p = current->GetPosition();
            current->Move(slack*cardnum ,genframe->getCardSlip());
            cardnum++;
        }
    }
    if(SlotPosStyle == LEFT || SlotPosStyle == RIGHT){
        int endline = this->GetSize().GetHeight() - cardlist.GetFirst()->GetData()->GetSize().GetHeight();
        int cardnum = cardlist.GetCount();
        int slack = endline/cardnum;
        CardList::iterator iter;
        cardnum = 0;
        for(iter = cardlist.begin(); iter != cardlist.end(); ++iter)
        {
            Card *current = *iter;
            wxPoint p = current->GetPosition();
            current->Move(0,slack*cardnum);
            current->SetHide(true);
            cardnum++;
        }
    }
}

void CardSlot::AddCard(Card *newCard)
{
    if(SlotPosStyle == BUTTOM){
        cardlist.Append(newCard);
        newCard->Reparent(this);
        int endline = this->GetSize().GetWidth() - newCard->GetSize().GetWidth();
        int cardnum = cardlist.GetCount();
        int slack = endline/cardnum;
        CardList::iterator iter;
        cardnum = 0;
        for(iter = cardlist.begin(); iter != cardlist.end(); ++iter)
        {
            Card *current = *iter;
            current->Move(slack*cardnum ,30);
            cardnum++;
        }
    }

    if(SlotPosStyle == LEFT || RIGHT){
        cardlist.Append(newCard);
        newCard->Reparent(this);
        int endline = this->GetSize().GetWidth() - newCard->GetSize().GetWidth();
        int cardnum = cardlist.GetCount();
        int slack = endline/cardnum;
        CardList::iterator iter;
        cardnum = 0;
        for(iter = cardlist.begin(); iter != cardlist.end(); ++iter)
        {
            Card *current = *iter;
            current->Move(30,slack*cardnum);
            cardnum++;
        }
    }
}

void CardSlot::SetSlotPos(SLOT_POS pos){
    SlotPosStyle = pos;
}

void CardSlot::ClearChose(){
    CardList::iterator iter;
    for(iter = cardlist.begin(); iter != cardlist.end(); ++iter)
    {
        Card *current = *iter;
        current->Reset();
    }
}
