/*************************************************************************
    > File Name: card.cpp
    > Author: ma6174
    > Mail: ma6174@163.com
    > Created Time: 2015年03月19日 星期四 09时28分02秒
 ************************************************************************/

#include<iostream>
#include "card.h"
#include <wx/gdicmn.h>
#include "globle.h"
#include <wx/listimpl.cpp>
WX_DEFINE_LIST(CardList);

Card::Card(wxWindow *parent,
		   wxWindowID id,
		   const wxBitmap &bitmap,
		   const wxPoint &pos,
		   const wxSize &size)
	:wxWindow(parent,id,pos,wxSize(50,70),wxBU_AUTODRAW)
{
    IsChose = false;
    BeHide = false;
}
void Card::OnEnter(wxMouseEvent& event)
{
    if(IsChose||BeHide)
        return;
	wxPoint p = this->GetPosition();
	this->Move(p+wxPoint(0,-genframe->getCardSlip()));
}
void Card::OnLeave(wxMouseEvent& event)
{
    if(IsChose||BeHide)
        return;
	wxPoint p = this->GetPosition();
	this->Move(p+wxPoint(0,genframe->getCardSlip()));
}
void Card::OnPaint(wxPaintEvent &event){
    wxWindowDC windc(this);
    wxBrush bash(wxColor(0,255,255));
    windc.SetBackground(bash);
    windc.Clear();

    if(!BeHide){
        windc.DrawText(wxT("K"),5,4);
    }

    wxSize cardlayout(50,100);
    wxPen pen(wxColor(0,0,0));
    pen.SetWidth(5);
    windc.DrawLine(3,3,cardlayout.GetWidth()-3,3);
    windc.DrawLine(3,3,3,cardlayout.GetHeight()-3);
    windc.DrawLine(cardlayout.GetWidth()-3,3,cardlayout.GetWidth()-3,cardlayout.GetHeight()-3);
    windc.DrawLine(3,cardlayout.GetHeight()-3,cardlayout.GetWidth()-3,cardlayout.GetHeight()-3);
}
void Card::OnMouseUP(wxMouseEvent &event){
    if(BeHide)
        return;
    IsChose = true;
}

void Card::SetHide(bool hide){
    this->BeHide = hide;
}

void Card::Reset(){
    if(IsChose){
        wxPoint p = this->GetPosition();
        this->Move(p+wxPoint(0,genframe->getCardSlip()));
        IsChose = false;
    }
}
void Card::OnDClick(wxMouseEvent &event){
    event.Skip(true);
}

wxBEGIN_EVENT_TABLE(Card,wxWindow)
	EVT_ENTER_WINDOW(Card::OnEnter)
	EVT_LEAVE_WINDOW(Card::OnLeave)
	EVT_PAINT(       Card::OnPaint)
    EVT_LEFT_UP(     Card::OnMouseUP)
    EVT_LEFT_DCLICK( Card::OnDClick )
wxEND_EVENT_TABLE()
